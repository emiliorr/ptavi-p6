#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys


MALFORMED = "400 Bad Request"
BADMETHOD = "405 Method Not Allowed"
OK = "200 OK"

class SIPHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """

    @staticmethod
    def split_adrr(addr):
        correct, user, ip = True, '', ''
        if not addr.startswith('sip:'):
            correct = False
        components = addr[4:].split('@')
        if len(components) != 2:
            correct = False
        user, ip = components
        return correct, user, ip

    def handle(self):
        msg = self.rfile.read()
        client = self.client_address
        print(f"Recibido de {str(client)}:")
        print(f"{msg.decode('utf-8')}.")
        components = msg.decode('utf-8').split()
        if len(components) != 3:
            result = MALFORMED
        else:
            method, addr, proto = components
            if method not in ('INVITE', 'BYE', 'ACK'):
                result = BADMETHOD
            else:
                correct, user, ip = self.split_adrr(addr)
                if not correct:
                    result = MALFORMED
                else:
                    result = OK
        if (result == OK) and (method == 'ACK'):
            # No hay que hacer nada
            pass
        else:
            # Envía la respuesta
            msg = f"SIP/2.0 {result}\r\n"
            self.wfile.write(msg.encode('utf-8'))
            print(f"Sent: {msg}.")

def parse_args():
    correct = True
    if len(sys.argv) != 4:
        correct = False
    else:
        ip, port, file = sys.argv[1:4]
        try:
            port = int(port)
        except ValueError:
            correct = False

    if not correct:
        sys.exit("Usage: python3 server.py <IP> <port> <audio_file>")
    else:
        return ip, port, file

def main():
    # Leemos argumentos de línea de comandos
    ip, port, file = parse_args()
    # Creamos servidor y escuchamos
    with socketserver.UDPServer((ip, port), SIPHandler) as serv:
        print("Listening...")
        serv.serve_forever()


if __name__ == "__main__":
    main()

