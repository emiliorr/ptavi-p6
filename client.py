#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente VoIP muy simple
"""

import socket
import sys


def parse_args():
    correct = True
    if len(sys.argv) != 3:
        correct = False
    else:
        if sys.argv[1] not in ('INVITE', 'BYE'):
            correct = False
        else:
            method = sys.argv[1]
            invited = sys.argv[2].split('@')
            if len(invited) != 2:
                correct = False
            else:
                user = invited[0]
                components = invited[1].split(':')
                if len(components) != 2:
                    correct = False
                else:
                    ip = components[0]
                    try:
                        port = int(components[1])
                    except ValueError:
                        correct = False

    if not correct:
        sys.exit("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")
    else:
        return method, user, ip, port

def main():
    # Leemos argumentos de línea de comandos
    method, user, ip, port = parse_args()
    # Creamos el socket y lo configuramos
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        # Componemos el mensaje
        request = f"{method} sip:{user}@{ip} SIP/2.0"

        # Enviamos el mensaje
        my_socket.sendto(request.encode('utf-8'), (ip, port))

        # Recibimos respuesta
        response = my_socket.recv(2048).decode('utf-8')
        lines = response.splitlines()
        first = lines.pop(0)
        if first == "SIP/2.0 200 OK":
            if method == 'INVITE':
                ack = f"ACK sip:{user}@{ip} SIP/2.0"
                print("Sending ACK:", ack)
                my_socket.sendto(ack.encode('utf-8'), (ip, port))
        else:
            print("Error")
        print("Recibido:")
        print(response + ".")

    print("Terminando programa...")


if __name__ == "__main__":
    main()

